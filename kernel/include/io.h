#ifndef IO_INCLUDED
#define IO_INCLUDED

#include "system.h"

class io
{
  public:
    //write a byte to a port
    static void outb(u16int port, u8int val);
    //read byte from a port
    static u8int inb(u16int port);
    //read a word from a port
    static u16int inw(u16int port);
};

#endif
