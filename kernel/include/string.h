#ifndef STRING_H
#define STRING_H

//copy char[] a<-b
void str_cpy(char* dest, char* src);

//a=?b
bool str_eq(char* a, char* b);

//returns length of char[]
//TODO: arbitrary array len?
int str_len(char* in);

//attempts to parse string to integer
//TODO: More numeric types
int str_to_int(char* s);

//count number of segments when string is split on delim char
int count_splits(char* s, char delim);

//get nth slice of string
//TODO: return 2d char array of all splits
char* get_split(char* s, char delim, int split);
#endif
