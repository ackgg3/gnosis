#ifndef common_h
#define common_h
 
#include <system.h>

#define OS_NAME "gnOSis"
#define OS_ARCH "x86"
#define USER    "osdev"
//initialize global descriptors
void init_desc_tables();
//fancy splash screen
void boot_text();
#endif
