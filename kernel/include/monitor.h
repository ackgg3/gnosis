#ifndef MONITOR_H_INCLUDED
#define MONITOR_H_INCLUDED

#include <system.h>

//VGA Color Codes
#define BLACK         0
#define BLUE          1
#define GREEN         2
#define CYAN          3
#define RED           4
#define MAGENTA       5
#define BROWN         6
#define LIGHT_GREY    7
#define DARK_GREY     8
#define LIGHT_BLUE    9
#define LIGHT_GREEN   10
#define LIGHT_CYAN    11
#define LIGHT_RED     12
#define LIGHT_MAGNETA 13
#define LIGHT_BROWN   14
#define WHITE         15

class monitor : io
{
  //VGA framebuffer starts at 0xB8000.
  u16int *video_memory;

  //cursor position.
  u8int cursor_x;
  u8int cursor_y;

  //no constructors, so we ini manually
  bool isInitialised;

public:
  //initial setup
  void init();
  //set cursor to (x,y)
  void seed(u8int x , u8int y);
  //clear screen
  void clear();
  //roll contents of screen up
  void scroll();
  //advance cursor position
  void movecursor();
  //write character to current cursor position
  void putch(char ch);
  //write string to current cursor postition
  void write(const char *c);
  //convert int to string and write to current cursor position
  void write_dec(int n);
  //colored putchar
  void cputch(char ch, u8int bg, u8int fg);
  //colored write
  void cwrite(const char *c, u8int bg, u8int fg);
  //colored write_dec
  void cwrite_dec(int n, u8int bg, u8int fg);

//oneline functions
bool isinit()
{
  return isInitialised;
}

u8int return_x()
{
  return cursor_x;
}

u8int return_y()
{
  return cursor_y;
}

};

//print to the screen
void printj(const char *s);
void putch(char ch);
//print a number
void print_dec(u32int n);
//clear the Screen
void clrscr();

//print to the screen
void cprintj(const char *s, u8int bg, u8int fg);
void cputch(char ch, u8int bg, u8int fg);
//print a colored number
void cprint_dec(u32int n, u8int bg, u8int fg);


#endif
