#include <system.h>

// Declared as a C function so as to remove name mangling and call it from the assembly file
extern "C" int kmain(struct multiboot *mboot_ptr);

// The Main Function
int kmain(struct multiboot *mboot_ptr)
{
  //make a fancy splash screen
  clrscr();
  boot_text();

  //init descritor tables
  init_desc_tables();

  //reenable Interrupts
  asm volatile("sti");

  //all device intialisations go here
  init_keyboard_driver();
  init_timer(0); 		//0 = disable timer
  printj("Color test:");
  for(int i=0; i < 16; i++)
  {
  	cprintj(" ",i,WHITE);
  }

  printj("\n                Version 0.3.03\n");	//actually update this sometime maybe?
  printj("================[System Ready]================\n");
  char* linein;
  while(true)
  {
  	putch('>');
	linein = keyboard_readline();

	if(str_eq(get_split(linein, ' ', 1), (char*)"halt"))
	{
		clrscr();
		//Add system cleanup here
		//and move to deboot function
		printj("\nIT IS SAFE TO POWER OFF");
		return 1;
	}
	//wrap this logic in a shell function?
	if(str_eq(get_split(linein, ' ', 1), (char*)"clear"))
	{
		clrscr();
	}
	else if(str_eq(get_split(linein,' ',1), (char*)"test"))
	{
		cprintj("TESTING EXPERIMENTAL FUNCTIONALITY\n", BLACK, RED);
		printj("NO TEST\n");
	}
  else if(str_eq(get_split(linein,' ',1), (char*)"file"))
  {
    printj("attempting to start file system");
  }
  else if(str_eq(get_split(linein,' ',1), (char*)"browse"))
  {
    printj("starting file browser");
  }
  else if(str_eq(get_split(linein,' ',1), (char*)"help")
		   || str_eq(get_split(linein, ' ',1),(char*)"h"))
	{
		printj("Commands:\n");
    printj("file  -- try file system! (WARNING BAD)");
		printj("test  -- whatever the current test function is\n");
		printj("clear -- clear the screen\n");
		printj("halt  -- stop the OS\n");
		printj("h/help-- this text, dummy\n");
	}
}

  return 1;
}
